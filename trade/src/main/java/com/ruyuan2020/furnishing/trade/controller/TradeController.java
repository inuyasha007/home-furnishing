package com.ruyuan2020.furnishing.trade.controller;

import com.ruyuan2020.common.domain.JsonResult;
import com.ruyuan2020.common.util.BeanCopierUtils;
import com.ruyuan2020.common.util.ResultHelper;
import com.ruyuan2020.furnishing.trade.domian.TradeDTO;
import com.ruyuan2020.furnishing.trade.domian.TradeQuery;
import com.ruyuan2020.furnishing.trade.domian.TradeVO;
import com.ruyuan2020.furnishing.trade.service.TradeService;
import com.scholar.sdk.utils.RollingWindow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/trade")
public class TradeController {

    @Autowired
    private TradeService tradeService;

    @Autowired
    private RollingWindow rollingWindow;

    /**
     * 分页查询
     *
     * @param tradeQuery 查询条件
     * @return 交易信息分页数据
     */
    @GetMapping("/page")
    public JsonResult<?> listByPage(TradeQuery tradeQuery) {
        return ResultHelper.ok(BeanCopierUtils.convert(tradeService.listByPage(tradeQuery), TradeVO.class));
    }

    /**
     * 获取交易信息
     *
     * @param id 交易id
     * @return 交易信息
     */
    @GetMapping("/{id}")
    public JsonResult<?> get(@PathVariable("id") Long id) {
        return ResultHelper.ok(tradeService.get(id).clone(TradeVO.class));
    }

    /**
     * 充值金币
     *
     * @param tradeVO 交易信息
     * @return 支付信息
     */
    @PostMapping("/gold")
    public JsonResult<?> rechargeGood(@RequestBody TradeVO tradeVO) {
        String payUrl = tradeService.rechargeGood(tradeVO.getMemberId(), tradeVO.clone(TradeDTO.class));
        return ResultHelper.ok(payUrl);
    }

    /**
     * 支付托管金
     *
     * @param tradeVO 交易信息
     * @return 支付信息
     */
    @PostMapping("/trust")
    public JsonResult<?> payTrust(@RequestBody TradeVO tradeVO) {
        rollingWindow.hit(1);
        String payUrl = tradeService.payTrust(tradeVO.clone(TradeDTO.class));
        return ResultHelper.ok(payUrl);
    }
}
