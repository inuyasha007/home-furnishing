package com.ruyuan2020.furnishing.payment.api.impl;

import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.payment.api.PaymentApi;
import com.ruyuan2020.furnishing.payment.domain.PaymentDTO;
import com.ruyuan2020.furnishing.payment.service.PaymentService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService(version = "1.0.0", interfaceClass = PaymentApi.class)
public class PaymentApiImpl implements PaymentApi {

    @Autowired
    private PaymentService paymentService;

    @Override
    public String buildPayUrl(PaymentDTO paymentDTO) throws BusinessException {
        return paymentService.buildPayUrl(paymentDTO);
    }
}
