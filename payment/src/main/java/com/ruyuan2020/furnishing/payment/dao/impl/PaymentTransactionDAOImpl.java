package com.ruyuan2020.furnishing.payment.dao.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.ruyuan2020.furnishing.common.dao.impl.BaseDAOImpl;
import com.ruyuan2020.furnishing.payment.dao.PaymentTransactionDAO;
import com.ruyuan2020.furnishing.payment.domain.PaymentTransactionDO;
import com.ruyuan2020.furnishing.payment.mapper.PaymentTransactionMapper;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class PaymentTransactionDAOImpl extends BaseDAOImpl<PaymentTransactionMapper, PaymentTransactionDO> implements PaymentTransactionDAO {

    @Override
    public Optional<PaymentTransactionDO> getByTradeNo(String tradeNo) {
        LambdaUpdateWrapper<PaymentTransactionDO> queryWrapper = new LambdaUpdateWrapper<>();
        queryWrapper.eq(PaymentTransactionDO::getTradeNo,tradeNo);
        return Optional.ofNullable(mapper.selectOne(queryWrapper));
    }
}
