package com.ruyuan2020.furnishing.tender.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan2020.furnishing.tender.domain.TenderDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TenderMapper extends BaseMapper<TenderDO> {
}
