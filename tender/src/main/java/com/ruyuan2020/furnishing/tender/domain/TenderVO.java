package com.ruyuan2020.furnishing.tender.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
public class TenderVO extends BaseDomain {

    private Long id;

    private Long categoryId;

    private BigDecimal budgetAmount;

    private BigDecimal trustAmount;

    private Boolean payFlag;

    private Long memberId;

    private String contact;

    private String mobile;

    private Long cityId;

    private Long areaId;

    private String addr;

    private String title;

    private String content;

    private String photo;

    private String status;

    private LocalDateTime gmtCreate;
}
