package com.ruyuan2020.furnishing.tender.service.state.impl;

import com.ruyuan2020.furnishing.member.api.MemberApi;
import com.ruyuan2020.furnishing.tender.constant.TenderStatus;
import com.ruyuan2020.furnishing.tender.dao.BiddingDAO;
import com.ruyuan2020.furnishing.tender.dao.TenderDAO;
import com.ruyuan2020.furnishing.tender.domain.TenderDO;
import com.ruyuan2020.furnishing.tender.domain.TenderDTO;
import com.ruyuan2020.furnishing.tender.service.state.TenderState;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 已签单状态
 */
@Component
public class SignedTenderState implements TenderState {

    @Autowired
    private TenderDAO tenderDAO;

    /**
     * 签约状态的流转
     *
     * @param tenderDTO 招标信息
     */
    @Override
    public void doTransition(TenderDTO tenderDTO) {
        tenderDTO.setStatus(TenderStatus.SIGNED_TENDER);
        TenderDO tenderDO = tenderDTO.clone(TenderDO.class);
        // 更新状态
        tenderDAO.update(tenderDO);
    }

    @Override
    public Boolean canBid(TenderDTO tenderDTO) {
        return false;
    }

    @Override
    public Boolean canSign(TenderDTO tenderDTO) {
        return false;
    }

    @Override
    public Boolean canComplete(TenderDTO tenderDTO) {
        return true;
    }

    @Override
    public Boolean canComment(TenderDTO tenderDTO) {
        return false;
    }
}
