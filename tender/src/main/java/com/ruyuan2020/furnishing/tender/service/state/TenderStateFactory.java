package com.ruyuan2020.furnishing.tender.service.state;

import com.ruyuan2020.furnishing.tender.constant.TenderStatus;
import com.ruyuan2020.furnishing.tender.dao.TenderDAO;
import com.ruyuan2020.furnishing.tender.domain.TenderDTO;
import com.ruyuan2020.furnishing.tender.service.state.impl.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 招标状态工厂
 */
@Component
public class TenderStateFactory {

    /**
     * 招标中状态
     */
    @Autowired
    private WaitingTenderState waitingTenderState;

    /**
     * 待提交状态
     */
    @Autowired
    private EndTenderState endTenderState;

    /**
     * 已签约状态
     */
    @Autowired
    private SignedTenderState signedTenderState;

    /**
     * 已完成工程
     */
    @Autowired
    private CompletionTenderState completionTenderState;

    /**
     * 默认招标信息状态
     */
    @Autowired
    private DefaultTenderState defaultTenderState;

    /**
     * 招标信息管理DAO组件
     */
    @Autowired
    private TenderDAO tenderDAO;

    /**
     * 获取招标信息对应的状态组件
     *
     * @param tenderDTO 招标信息
     * @return 状态组件
     */
    public TenderState get(TenderDTO tenderDTO) {
        if (StringUtils.isBlank(tenderDTO.getStatus())) {
            String status = tenderDAO.getStatus(tenderDTO.getId());
            tenderDTO.setStatus(status);
        }
        if (TenderStatus.WAITING_TENDER.equals(tenderDTO.getStatus())) {
            return waitingTenderState;
        } else if (TenderStatus.END_TENDER.equals(tenderDTO.getStatus())) {
            return endTenderState;
        } else if (TenderStatus.SIGNED_TENDER.equals(tenderDTO.getStatus())) {
            return signedTenderState;
        } else if (TenderStatus.COMPLETED_TENDER.equals(tenderDTO.getStatus())) {
            return completionTenderState;
        } else {
            return defaultTenderState;
        }
    }

}
