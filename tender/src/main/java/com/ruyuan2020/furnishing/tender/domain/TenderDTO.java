package com.ruyuan2020.furnishing.tender.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
public class TenderDTO extends BaseDomain {

    /**
     * 招标id
     */
    private Long id;

    /**
     * 招标类别
     */
    private Long categoryId;

    /**
     * 预算金额
     */
    private BigDecimal budgetAmount;

    /**
     * 托管金
     */
    private BigDecimal trustAmount;

    /**
     * 是否支付托管金
     */
    private Boolean payFlag;

    /**
     * 招标会员id
     */
    private Long memberId;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 市id
     */
    private Long cityId;

    /**
     * 区id
     */
    private Long areaId;

    /**
     * 地址
     */
    private String addr;

    /**
     * 需求标题
     */
    private String title;

    /**
     * 需求内容
     */
    private String content;

    /**
     * 照片
     */
    private String photo;

    /**
     * 状态
     */
    private String status;

    /**
     * 最大投标数
     */
    private Integer biddingMaxCount;

    private LocalDateTime gmtCreate;

    private BiddingDTO bidding;
}
