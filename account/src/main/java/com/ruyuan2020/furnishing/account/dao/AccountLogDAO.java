package com.ruyuan2020.furnishing.account.dao;

import com.ruyuan2020.furnishing.common.dao.BaseDAO;
import com.ruyuan2020.furnishing.account.domain.AccountLogDO;

public interface AccountLogDAO extends BaseDAO<AccountLogDO> {
}
