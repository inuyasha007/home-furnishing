package com.ruyuan2020.furnishing.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan2020.furnishing.account.domain.AccountDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

@Mapper
public interface AccountMapper extends BaseMapper<AccountDO> {

    @Update("update ry_account set gold=gold+#{gold},point=point+#{point} where member_id=#{id}")
    void addGoldAndPoint(@Param("id") Long id, @Param("gold") BigDecimal gold, @Param("point") BigDecimal point);

    @Update("update ry_account set gold=gold-#{gold},freeze_gold=freeze_gold+#{gold} where member_id=#{id}")
    void freezeGold(@Param("id") Long id, @Param("gold") BigDecimal gold);

    @Update("update ry_account set freeze_gold=freeze_gold-#{gold} where member_id=#{id}")
    void subtractFreezeGold(@Param("id") Long id, @Param("gold") BigDecimal gold);

    @Update("update ry_account set gold=gold-#{gold} where member_id=#{id}")
    void subtractGold(@Param("id") Long id, @Param("gold") BigDecimal gold);
}
